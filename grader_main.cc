#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>


using namespace std;


#include <string>
#include <iostream>

// 0~100점의 성적을 학점으로 변환하는 클래스. 과목 이름과 시수를 저장.
class Subject {
public:
  Subject(const string& name, int credit) : name_(name), credit_(credit) {}
  virtual ~Subject() {}
  const string& name() const { return name_; }
  int credit() const { return credit_; }
  virtual string GetGrade(int score) const = 0;
private:
  string name_;
  int credit_;
};

// 성적을 Pass / Fail 로 구분하여 출력해주는 클래스.
// 성적이 pass_score보다 같거나 높으면 "P", 아니면 "F"를 리턴.
class SubjectPassFail : public Subject {
public:
  SubjectPassFail(const string& name, int credit, int pass_score)
	 : Subject(name, credit), pass_score_(pass_score){}

  virtual ~SubjectPassFail(){}
  virtual string GetGrade(int score) const{
    if(score >= pass_score_){ return "P";
    } else return "F";
  }
private:
  string name;
  int pass_score_;
};

// 성적을 A, B, C, D, F 로 구분하여 출력해주는 클래스.
// 성적이 속하는 구간에 따라
// 100 >= "A" >= cutA > "B" >= cutB > "C" >= cutC > "D" >= cutD > "F".
class SubjectGrade : public Subject {
public:
  SubjectGrade(const string& name, int credit,
              int cutA, int cutB, int cutC, int cutD)
   : Subject(name, credit), cutA_(cutA), cutB_(cutB),cutC_(cutC),cutD_(cutD){}


  virtual ~SubjectGrade(){}
  virtual string GetGrade(int score) const{
    if(score > 100){ return "ERROR";
    } else if(score >= cutA_){ return "A";
    } else if(score >= cutB_){ return "B";
    } else if(score >= cutC_){ return "C";
    } else if(score >= cutD_){ return "D";
    } else return "F";
  }
private:
  int cutA_, cutB_, cutC_, cutD_;
};






inline bool CompareStudent(const pair<string, double>& a,
                           const pair<string, double>& b) {
  return (a.second > b.second);
}

double GetNumberGrade(const string& str) {
  if (str == "A" || str == "P") return 4.0;
  if (str == "B") return 3.0;
  if (str == "C") return 2.0;
  if (str == "D") return 1.0;
  return 0.0;
}




int main() {
  SubjectPassFail subject1("Seminar", 1, 70);
  SubjectGrade subject2("C++", 6, 90, 80, 70, 60);
  SubjectGrade subject3("Calculus", 3, 80, 60, 40, 20);
  SubjectGrade subject4("Statistics", 2, 80, 70, 60, 50);
  const int total_credit = subject1.credit() + subject2.credit()
                         + subject3.credit() + subject4.credit();
  string cmd;
  vector<pair<string, double> > student_grades;
  while (cmd != "quit") {
    cin >> cmd;
    if (cmd == "eval") {
      string name;
      int score1, score2, score3, score4;
      cin >> name >> score1 >> score2 >> score3 >> score4;
      string grade1 = subject1.GetGrade(score1);
      string grade2 = subject2.GetGrade(score2);
      string grade3 = subject3.GetGrade(score3);
      string grade4 = subject4.GetGrade(score4);
     cout << name << " " << grade1 << " " << grade2
          << " " << grade3 << " " << grade4 << endl;
      double average_grade =
          (GetNumberGrade(grade1) * subject1.credit() +
           GetNumberGrade(grade2) * subject2.credit() +
           GetNumberGrade(grade3) * subject3.credit() +
           GetNumberGrade(grade4) * subject4.credit()) / total_credit;
      student_grades.push_back(make_pair(name, average_grade));
    }
  }

  sort(student_grades.begin(), student_grades.end(), CompareStudent);

  for (int i = 0; i < student_grades.size(); ++i) {
    // 여기에서 학점 출력이 소숫점 두자리만 되도록 cout의 멤버함수 호출
    cout << student_grades[i].first << " "
         << setprecision(3) << student_grades[i].second << endl;
  }
  return 0;
}




















