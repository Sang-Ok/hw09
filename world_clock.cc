#include "world_clock.h"
#include <vector>


string itostr(int i){
  string ret;
  vector<char> v;
  for(;0<i;i/=10) v.push_back('0'+i%10);
  for(i=v.size(); 0<=i; i--) ret+=v[i];
  return ret;
}

WorldClock& WorldClock::AddTimezoneInfo(const string& name, const int& hour){
    timezones_[name]=hour;
    return *this;
}
void WorldClock::SetTimezone(const string& name){
  if(0 < timezones_.count(name))
      timezone_now_=name;
  else
      throw InvalidTimeException("no timezone named "+name);
}


istream& operator >>(istream& is, WorldClock& w){
    int h,m,s;
    char a1,a2;
    is>>h>>a1>>m>>a2>>s;
    if( a1 != a2 || a2 != ':' ||
        h < 0 || 24 <= h ||  m < 0 || 60 <= m || s < 0 || 60 <= s ){
      string message = itostr(h)+a1+itostr(m)+a2+itostr(s);
      throw InvalidTimeException(message);
    }else{
      w.tick() = s +( m + h *60)*60;
    }
    return is;
}

ostream& operator <<(ostream& os, WorldClock& w){
    w.discard24();
    int h,m,s, temp=w.tick(), div=60;
    temp += w.timezones()[w.timezone_now()]*60*60;
//    if(0 <= w.timezone_now())
//        temp += w.timezone_now()*60*60;
//    else
//        return os<<"invalid timezone";

    s = (temp % div),        temp/=div;
    m = (temp % div),        temp/=div;
    h = (temp % div);
    if(w.timezone_now()=="GMT")
      return os<<h<<':'<<m<<':'<<s;
    else
      return os<<h<<':'<<m<<':'<<s<<" (+"<<w.timezones()[w.timezone_now()]<<')';
  };


