#include <iostream>
#include <string>
#include <map>
#include <cstdlib>
using namespace std;

typedef string InvalidTimeException;
typedef char FileFinish;

class WorldClock{
public:
  WorldClock():tick_(0),timezone_now_("GMT"){}
  ~WorldClock(){}
  int& tick(){return tick_;}
  void Tick(int tick){tick_+=tick;}
  void SetTimezone(const string& name);
  WorldClock& AddTimezoneInfo(const string& name, const int& hour);
  const string& timezone_now() {return timezone_now_;}
  void discard24(){
    while(tick_<0) tick_+=(24*60*60);
    tick_%=(24*60*60);
  }
  map<string,int>& timezones(){return timezones_;}
private:
  int tick_;
  string timezone_now_;
  map<string,int> timezones_;
};

istream& operator >>(istream& is, WorldClock& w);
ostream& operator <<(ostream& os, WorldClock& w);
